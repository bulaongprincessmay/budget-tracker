import { useState, useEffect } from "react";
import "../styles/globals.css";
import NavBar from "../components/NavBar";
import { UserProvider } from "../UserContext";
import { Container } from "react-bootstrap";

export default function MyApp({ Component, pageProps }) {
  const [user, setUser] = useState({
    id: null,
  });

  // functions as a logout button

  useEffect(() => {
    fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        if (data._id) {
          setUser({
            id: data._id,
          });
        } else {
          setUser({
            id: null,
          });
        }
      });
  }, [user.id]);

  const unsetUser = () => {
    localStorage.clear();

    setUser({
      id: null,
    });
  };

  return (
    <React.Fragment>
      <UserProvider value={{ user, setUser, unsetUser }}>
        <NavBar />
        <Container>
          <Component {...pageProps} />
        </Container>
      </UserProvider>
    </React.Fragment>
  );
}
