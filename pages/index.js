import Head from "next/head";
import Landing from "./landing";

import styles from "../styles/Home.module.css";

export default function Home() {
  return (
    <React.Fragment>
      <Head>
        <title>Budget Tracker</title>
      </Head>
      <Landing />
    </React.Fragment>
  );
}
