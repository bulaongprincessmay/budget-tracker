import { useState } from "react";
import React from "react";
import { Col, Card, Form, Button } from "react-bootstrap";
import Router from "next/router";
import UserContext from "../UserContext";

export default function addexpensescategory() {
  // const { user } = useContext(UserContext);
  const [categoryName, setCategoryName] = useState("");

  function addCategory(e) {
    e.preventDefault();
    fetch("http://localhost:4000/api/users/expensescategories", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        categoryName: categoryName,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data === true) {
          Router.push("/addexpenses");
        } else {
          Router.push("/error");
        }
      });
  }

  return (
    <Col className="col-lg-4 offset-lg-4 my-5">
      <Card>
        <Card.Body>
          <h2 className="text-center mb-5 text-primary">Expenses Category</h2>

          <Form onSubmit={(e) => addCategory(e)}>
            <Form.Group as={Col} controlId="categoryName">
              <Form.Label>Select Options</Form.Label>
              <Form.Control as="select">
                <option>Add</option>
              </Form.Control>
            </Form.Group>
            <Form.Group as={Col}>
              <Form.Label>Category Name</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter category name"
                value={categoryName}
                onChange={(e) => setCategoryName(e.target.value)}
                required
              />
            </Form.Group>

            <Button className="bg-primary offset-lg-9" type="submit">
              SUBMIT
            </Button>
          </Form>
        </Card.Body>
      </Card>
    </Col>
  );
}
