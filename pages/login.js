import { useState, useContext } from "react";
import { Form, Button, Row, Col, Card } from "react-bootstrap";
import { GoogleLogin } from "react-google-login";
import Swal from "sweetalert2";

import Router from "next/router";
import Head from "next/head";

import UserContext from "../UserContext";

import View from "../components/View";
import AppHelper from "../app-helper";

export default function login() {
  return (
    <View title={"Login"}>
      <Row>
        <Col>
          <LoginForm />
        </Col>
      </Row>
    </View>
  );
}

const LoginForm = () => {
  // const [user, setUser] = useState('');
  const { setUser } = useContext(UserContext);

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const [tokenId, setTokenId] = useState(null);

  function authenticate(e) {
    //prevent redirection via form submission
    e.preventDefault();

    const options = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    };
    fetch(`${AppHelper.API_URL}/users/login`, options)
      .then(AppHelper.toJSON)
      .then((data) => {
        if (typeof data.accessToken !== "undefined") {
          localStorage.setItem("token", data.accessToken);
          retrieveUserDetails(data.accessToken);
        } else {
          if (data.error === "does-not-exist") {
            Swal.fire("Authenication Failed", "User does not exist", "error");
          } else if (data.error === "incorrect-password") {
            Swal.fire("Authenication Failed", "User does not exist", "error");
          } else if (data.error === "login-type-error") {
            Swal.fire(
              "Login Type Error",
              "You may have registered through a different login procedure",
              "error"
            );
          }
        }
      });
  }
  const retrieveUserDetails = (accessToken) => {
    const options = {
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    };

    fetch(`${AppHelper.API_URL}/users/details`, options)
      .then(AppHelper.toJSON)
      .then((data) => {
        setUser({
          id: data._id,
          isAdmin: data.isAdmin,
        });

        Router.push("/currentwallet");
      });
  };

  return (
    <Col className="col-lg-4 offset-lg-4 my-5">
      <Card>
        <Card.Body>
          <h2 className="text-center mb-5 text-primary">Login</h2>

          <Form onSubmit={(e) => authenticate(e)}>
            <Form.Group as={Col}>
              <Form.Label>Email</Form.Label>
              <Form.Control
                type="email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                required
              />
            </Form.Group>

            <Form.Group as={Col}>
              <Form.Label>Password</Form.Label>
              <Form.Control
                type="password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                required
              />
            </Form.Group>

            <Button className="bg-primary offset-lg-8" type="submit">
              SIGN IN
            </Button>
          </Form>
        </Card.Body>
      </Card>
    </Col>
  );
};

/*
  const authenticateGoogleToken = (response) => {
    console.log(response);

    const payload = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ tokenId: response.tokenId }),
    };
    fetch(`${AppHelper.API_URL}/users/login`, options)
      .then(AppHelper.toJSON)
      .then((data) => {
        //typeof - checking for specific data
        if (typeof data.accessToken !== "undefined") {
          localStorage.setItem("token", data.accessToken);
          retrieveUserDetails(data.accessToken);
        } else {
          if (data.error === "does-not-exist") {
            Swal.fire("Authentication Failed", "User does not exist.", "error");
          } else if (data.error === "incorrect-password") {
            Swal.fire(
              "Authentication Failed",
              "Password is incorrect.",
              "error"
            );
          } else if (data.error === "login-type-error") {
            Swal.fire(
              "Login Type Error",
              "You may have registered through a different login procedure",
              "error"
            );
          }
        }
      });
  };

  const retrieveUserDetails = (accessToken) => {
    const options = {
      headers: { Authorization: `Bearer ${accessToken}` },
    };
    fetch(` ${AppHelper.API_URL}/users/details`, options)
      .then(AppHelper.toJSON)
      .then((data) => {
        setUser({
          id: data._id,
          isAdmin: data.isAdmin,
        });
        Router.push("/courses");
      });
  };

  return (
    <React.Fragment>
      <Card>
        <Card.Header>Login Detials</Card.Header>
        <Card.Body>
          <Form onSubmit={(e) => authenticate(e)}>
            <Form.Group controlId="userEmail">
              <Form.Label>Email address</Form.Label>
              <Form.Control
                type="email"
                placeholder="Enter email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                required
              />
            </Form.Group>

            <Form.Group controlId="password">
              <Form.Label>Password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                required
              />
            </Form.Group>

            <Button className="bg-primary" type="submit">
              Submit
            </Button>
          </Form>
        </Card.Body>
      </Card>
    </React.Fragment>
  );
};
 */
