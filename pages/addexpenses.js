import { useContext, useState, useEffect } from "react";
import React from "react";
import { Col, Card, Form, Button } from "react-bootstrap";

export default function addexpenses() {
  //const { user } = useContext(UserContext);
  const [expensesCategory, setExpensesCategory] = useState([]);

  /* function income(e) {
    fetch(`http://localhost:4000/api/users/details`)
      .then((res) => {
        return res.json();
      })
      .then((data) => {
        setIncomeCategorys(data.categoryIncomeName);
      });
  } */
  useEffect(() => {
    fetch(`http://localhost:4000/api/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        if (data._id) {
          setExpensesCategory(data.categories);
        } else {
          setExpensesCategory([]);
        }
      });
  }, []);

  return (
    <Col className="col-lg-5 offset-lg-4 my-5">
      <Card>
        <Card.Body>
          <h2 className="text-center mb-5 text-primary">Current Wallet</h2>

          <h1 className="text-center mb-5 text-light bg-primary"> P 100,000</h1>

          <Form>
            <Form.Group>
              <Form.Label>Expenses Category</Form.Label>
              <Form.Control as="select">
                {expensesCategory.map((expenseCategory) => {
                  return (
                    <option key={expenseCategory.categoryName}>
                      {expenseCategory.categoryName}
                    </option>
                  );
                })}
              </Form.Control>
            </Form.Group>

            <Form.Row>
              <Form.Group as={Col}>
                <Form.Label>Amount</Form.Label>
                <Form.Control type="number" />
              </Form.Group>
            </Form.Row>

            <Form.Row>
              <Form.Group as={Col}>
                <Form.Label>Remarks</Form.Label>
                <Form.Control type="text" />
              </Form.Group>
            </Form.Row>
            <Button className="bg-primary offset-lg-9" type="submit">
              SUBMIT
            </Button>
          </Form>
        </Card.Body>
      </Card>
    </Col>
  );
}
