import { useState, useEffect } from "react";
import { Col, Card, Form, Button } from "react-bootstrap";
import Router from "next/router";

export default function signup() {
  const [email, setEmail] = useState("");
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [password1, setPassword1] = useState("");
  const [password2, setPassword2] = useState("");
  const [isActive, setIsActive] = useState(false);

  function registerUser(e) {
    e.preventDefault();

    // used to send requests
    // fetch('url', {options})

    // check for duplicate email
    fetch("http://localhost:4000/api/users/email-exists", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: email,
      }),
    })
      .then((res) => {
        return res.json();
      })
      .then((data) => {
        //if no duplicates found -> register
        if (data === false) {
          fetch("http://localhost:4000/api/users", {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify({
              firstName: firstName,
              lastName: lastName,
              email: email,
              password: password1,
            }),
          })
            .then((res) => {
              return res.json();
            })
            .then((data) => {
              if (data === true) {
                Router.push("/login");
              } else {
                Router.push("/errors/1");
              }
            });
        } else {
          Router.push("/errors/2");
        }
      });
  }
  useEffect(() => {
    if (
      email !== "" &&
      password1 !== "" &&
      password2 !== "" &&
      password1 === password2
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password1, password2]);
  return (
    <Col className="col-lg-6 offset-lg-4 my-5">
      <Card>
        <Card.Body>
          <h2 className="text-center mb-5 text-primary">Enter Details</h2>

          <Form onSubmit={(e) => registerUser(e)}>
            <Form.Group as={Col}>
              <Form.Label>Email</Form.Label>
              <Form.Control
                type="email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                required
              />
            </Form.Group>
            <Form.Group as={Col}>
              <Form.Label>First Name</Form.Label>
              <Form.Control
                type="text"
                value={firstName}
                onChange={(e) => setFirstName(e.target.value)}
                required
              />
            </Form.Group>
            <Form.Group as={Col}>
              <Form.Label>Last Name</Form.Label>
              <Form.Control
                type="text"
                value={lastName}
                onChange={(e) => setLastName(e.target.value)}
                required
              />
            </Form.Group>
            <Form.Group as={Col}>
              <Form.Label>Password</Form.Label>
              <Form.Control
                type="password"
                value={password1}
                onChange={(e) => setPassword1(e.target.value)}
                required
              />
            </Form.Group>

            <Form.Group as={Col}>
              <Form.Label>Verify Password</Form.Label>
              <Form.Control
                type="password"
                value={password2}
                onChange={(e) => setPassword2(e.target.value)}
                required
              />
            </Form.Group>
            {isActive ? (
              <Button className="bg-primary offset-lg-9" type="submit">
                SIGN UP
              </Button>
            ) : (
              <Button className="bg-primary offset-lg-9" type="submit" disabled>
                SIGN UP
              </Button>
            )}
          </Form>
        </Card.Body>
      </Card>
    </Col>
  );
}
