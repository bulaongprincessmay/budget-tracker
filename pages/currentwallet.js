import React from "react";
import { Col, Card, Form, Button } from "react-bootstrap";

export default function addincome() {
  return (
    <Col className="col-lg-8 offset-lg-2 my-5">
      <Card>
        <Card.Body>
          <h2 className=" mb-5 text-primary">Current Wallet</h2>

          <h1 className="text-center mb-5 text-light bg-primary"> P 100,000</h1>

          <Form className="my-5">
            <Form.Row>
              <Form.Group as={Col}>
                <Form.Label>
                  <h4 className="text-success">Total Income</h4>
                </Form.Label>
                <Form.Control type="text" />
              </Form.Group>

              <Form.Group as={Col}>
                <Form.Label>
                  <h4 className="text-danger">Total Expenses</h4>
                </Form.Label>
                <Form.Control type="number" />
              </Form.Group>
            </Form.Row>
          </Form>
        </Card.Body>
      </Card>
      <Card>
        <Card.Body>
          <h4>Search Logs</h4>

          <Form className="my-5">
            <Form.Row>
              <Form.Group as={Col}>
                <Form.Label>Filter</Form.Label>
                <Form.Control as="select">
                  <option>Income</option>
                  <option>Expenses</option>
                  <option>All Transactions</option>
                </Form.Control>
              </Form.Group>
              <Form.Group as={Col}>
                <Form.Label>Date Start</Form.Label>
                <Form.Control type="date" />
              </Form.Group>
              <Form.Group as={Col}>
                <Form.Label>Date End</Form.Label>
                <Form.Control type="date" />
              </Form.Group>
            </Form.Row>
            <Form.Row>
              <Button className="col-lg-4 offset-lg-8">Search</Button>
            </Form.Row>
          </Form>
        </Card.Body>
      </Card>
    </Col>
  );
}
