import { useContext, useState, useEffect } from "react";
import React from "react";
import { Col, Card, Form, Button } from "react-bootstrap";
import Router from "next/router";

export default function addincome() {
  //const { user } = useContext(UserContext);
  const [incomeCategorys, setIncomeCategorys] = useState([]);
  const [amount, setAmount] = useState(0);
  const [categoryId, setCategoryId] = useState(0);
  const [remarks, setRemarks] = useState(0);

  /* function income(e) {
    fetch(`http://localhost:4000/api/users/details`)
      .then((res) => {
        return res.json();
      })
      .then((data) => {
        setIncomeCategorys(data.categoryIncomeName);
      });
  } */
  function addIncomeTransaction(e) {
    e.preventDefault();
    fetch("http://localhost:4000/api/users/incomecategories", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        categoryIncomeName: categoryIncomeName,
        amount: amount,
        remarks: remarks,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data === true) {
          Router.push("/currentwallet");
        } else {
          Router.push("/error");
        }
      });
  }
  useEffect(() => {
    fetch(`http://localhost:4000/api/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        if (data._id) {
          setIncomeCategorys(data.categoriesIncome);
        } else {
          setIncomeCategorys([]);
        }
      });
  }, []);

  return (
    <Col className="col-lg-5 offset-lg-4 my-5">
      <Card>
        <Card.Body>
          <h2 className="text-center mb-5 text-primary">Current Wallet</h2>

          <h1 className="text-center mb-5 text-light bg-primary"> P 100,000</h1>

          <Form onSubmit={(e) => addIncomeTransaction(e)}>
            <Form.Group>
              <Form.Label>Income Category</Form.Label>
              <Form.Control as="select">
                {incomeCategorys.map((incomeCategory) => {
                  return (
                    <option key={incomeCategory.categoryIncomeName}>
                      {incomeCategory.categoryIncomeName}
                    </option>
                  );
                })}
              </Form.Control>
            </Form.Group>

            <Form.Row>
              <Form.Group as={Col}>
                <Form.Label>Amount</Form.Label>
                <Form.Control
                  type="number"
                  value={amount}
                  onChange={(e) => setAmount(e.target.value)}
                />
              </Form.Group>
            </Form.Row>
            <Form.Row>
              <Form.Group as={Col}>
                <Form.Label>Remarks</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="remarks"
                  value={remarks}
                  onChange={(e) => setRemarks(e.target.value)}
                />
              </Form.Group>
            </Form.Row>

            <Button className="bg-primary offset-lg-9" type="submit">
              SUBMIT
            </Button>
          </Form>
        </Card.Body>
      </Card>
    </Col>
  );
}
