import { useState, useContext } from "react";
import React from "react";

import { GoogleLogin } from "react-google-login";
import Link from "next/link";
import AppHelper from "../app-helper";
import UserContext from "../UserContext";
import Router from "next/router";
import { Col, Card, Container, Button } from "reactstrap";

export default function landing() {
  const { setUser } = useContext(UserContext);

  const authenticateGoogleToken = (response) => {
    console.log(response);

    const payload = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ tokenId: response.tokenId }),
    };
    fetch(`${AppHelper.API_URL}/users/verify-google-id-token`, payload)
      .then(AppHelper.toJSON)
      .then((data) => {
        //typeof - checking for specific data
        if (typeof data.accessToken !== "undefined") {
          localStorage.setItem("token", data.accessToken);
          retrieveUserDetails(data.accessToken);
        } else {
          if (data.error === "google-auth-error") {
            Swal.fire(
              "Google Auth Error",
              "Google authentication procedure failed",
              "error"
            );
          } else if (data.error === "login-type-error") {
            Swal.fire(
              "Login Type Error",
              "You may have registered through a different login procedure",
              "error"
            );
          }
        }
      });
  };

  const retrieveUserDetails = (accessToken) => {
    const options = {
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    };

    fetch(`${AppHelper.API_URL}/users/details`, options)
      .then(AppHelper.toJSON)
      .then((data) => {
        setUser({
          id: data._id,
          isAdmin: data.isAdmin,
        });

        Router.push("/currentwallet");
      });
  };
  return (
    <Container>
      <Card className="col-lg-4 offset-lg-4 my-5 align-items-center">
        <Card.Body>
          <h2 className="text-center mb-5 text-primary">Welcome!</h2>
          <div>
            <Link href="/signup">
              <Button size="lg" className="mb-3 w-100">
                Sign Up
              </Button>
            </Link>
          </div>
          <div>
            <Link href="/login">
              <Button size="lg" className="mb-3 w-100">
                Log In
              </Button>
            </Link>
          </div>
          <div>
            <GoogleLogin
              clientId="483450561397-gh05hlfh0crt5dv6eel2tfu56f12i1q6.apps.googleusercontent.com"
              buttonText="Login"
              onSuccess={authenticateGoogleToken}
              onFailure={authenticateGoogleToken}
              cookiePloicy={"single_host_origin"}
              className="w-100 text-center d-flex justify-content-center"
            />
          </div>
        </Card.Body>
      </Card>
    </Container>
  );
}
