import { useState } from "react";
import React from "react";
import { Col, Card, Form, Button } from "react-bootstrap";
import Router from "next/router";

export default function addincomecategory() {
  const [categoryIncomeName, setCategoryIncomeName] = useState("");

  function addIncomeCategory(e) {
    e.preventDefault();
    fetch("http://localhost:4000/api/users/incomecategories", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        categoryIncomeName: categoryIncomeName,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data === true) {
          Router.push("/addincome");
        } else {
          Router.push("/error");
        }
      });
  }
  return (
    <Col className="col-lg-4 offset-lg-4 my-5">
      <Card>
        <Card.Body>
          <h2 className="text-center mb-5 text-primary">Income Category</h2>

          <Form onSubmit={(e) => addIncomeCategory(e)}>
            <Form.Group as={Col} controlId="category2Name">
              <Form.Label>Select Options</Form.Label>
              <Form.Control as="select">
                <option>Add</option>
              </Form.Control>
            </Form.Group>
            <Form.Group as={Col}>
              <Form.Label>Category Name</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter category name"
                value={categoryIncomeName}
                onChange={(e) => setCategoryIncomeName(e.target.value)}
                required
              />
            </Form.Group>

            <Button className="bg-primary offset-lg-9" type="submit">
              SUBMIT
            </Button>
          </Form>
        </Card.Body>
      </Card>
    </Col>
  );
}
