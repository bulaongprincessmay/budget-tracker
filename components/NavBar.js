import { useContext } from "react";

import { Navbar, Nav, NavDropdown, Container } from "reactstrap";
import Link from "next/link";
import UserContext from "../UserContext";

export default function NavBar() {
  const { user } = useContext(UserContext);
  return (
    <Navbar bg="primary" variant="dark" expand="lg">
      <Container>
        <Navbar.Brand href="/">Budget Tracking App</Navbar.Brand>
        {user.id !== null ? (
          <React.Fragment>
            <Nav className="mr-auto">
              <Navbar.Toggle aria-controls="basic-navbar-nav" />

              <NavDropdown title="Income" id="basic-nav-dropdown">
                <NavDropdown.Item href="/addincome">
                  Add Income
                </NavDropdown.Item>

                <NavDropdown.Item href="/addincomecategory">
                  Add/Remove Income Category
                </NavDropdown.Item>
              </NavDropdown>
              <NavDropdown title="Expenses" id="basic-nav-dropdown">
                <NavDropdown.Item href="addexpenses">
                  Add Expenses
                </NavDropdown.Item>
                <NavDropdown.Item href="addexpensescategory">
                  Add/Remove Expenses Category
                </NavDropdown.Item>
              </NavDropdown>
              <NavDropdown title="Current Wallet" id="basic-nav-dropdown">
                <NavDropdown.Item href="/currentwallet">
                  Current Wallet
                </NavDropdown.Item>
                <NavDropdown.Item>Search Transactions</NavDropdown.Item>
              </NavDropdown>
              <NavDropdown title="History" id="basic-nav-dropdown">
                <NavDropdown.Item>Recent</NavDropdown.Item>
                <NavDropdown.Item>Monthly View</NavDropdown.Item>
              </NavDropdown>
            </Nav>
            <Nav>
              <NavDropdown title="Welcome" id="basic-nav-dropdown">
                <NavDropdown.Item href="/logout">Logout</NavDropdown.Item>
              </NavDropdown>
            </Nav>
          </React.Fragment>
        ) : (
          <React.Fragment>
            <Link href="/signup">
              <a className="nav-link text-light" role="button">
                Register
              </a>
            </Link>
          </React.Fragment>
        )}
      </Container>
    </Navbar>
  );
}
